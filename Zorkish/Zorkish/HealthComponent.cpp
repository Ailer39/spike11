#include "HealthComponent.h"

const string HealthComponent::type = "Health";

HealthComponent::HealthComponent(int startHealth)
{
	_health = startHealth;
}

int HealthComponent::GetHealth()
{
	return _health;
}

void HealthComponent::ChangeHealth(int amount)
{
	_health += amount;
}

void HealthComponent::PrintHealth()
{
	cout << "Health: " << _health << endl;
}