#pragma once
#include "StateBase.h"
#include "LevelReader.h"
#include <vector>

class SelectAdventure :
	public StateBase
{
private:
	SelectAdventure() {};
public:
	static SelectAdventure* GetInstance();
	void Show(StateManager* context);
};