#include "LevelReader.h"

Location* LevelReader::GetLevel()
{
	Location* result = nullptr;
	return result;
}

string LevelReader::ReadFile()
{
	ifstream fileStream;
	fileStream.open(_path);

	if (fileStream.is_open())
	{
		stringstream tmpStream;
		tmpStream << fileStream.rdbuf();
		return tmpStream.str();
	}

	return "";
}

vector<int> LevelReader::GetConnections()
{
	vector<int> result;

	return result;
}

Location* LevelReader::FindLocationById(int id)
{
	for (int i = 0; i < _locations.size(); i++)
	{
		if (_locations[i]->GetId() == id)
		{
			return _locations[i];
		}
	}

	return nullptr;
}

void LevelReader::CreateLevelObjects(GameObject* parent, const Value& jsonString)
{
	if (jsonString.IsArray() &&
		!jsonString.Empty())
	{
		string name;
		Document subJsonDoc;
		GameObject* gameObj;

		for (int n = 0; n < jsonString.Size(); n++)
		{
			gameObj = new GameObject(jsonString[n]["Name"].GetString(),
				jsonString[n]["Description"].GetString());
			parent->AddGameObject(gameObj);

			if (jsonString[n].HasMember("Live"))
			{
				gameObj->AddComponent(HealthComponent::type, 
									  new HealthComponent(jsonString[n]["Live"].GetInt()));
			}

			if (jsonString[n].HasMember("Objects"))
			{
				const Value& childObjects = jsonString[n]["Objects"];
				CreateLevelObjects(gameObj, childObjects);
			}
		}
	}
}

vector<Location*> LevelReader::GetLevelGraph()
{
	_locations.clear();
	string levelText = ReadFile();
	Document doc;
	doc.Parse(levelText.c_str());

	if (doc["Locations"].IsArray())
	{
		const Value& jsonSrc = doc["Locations"];
		string name;
		Location* tmp;

		// Create locations
		for (int i = 0; i < jsonSrc.Size(); i++)
		{
			tmp = new Location(jsonSrc[i]["Id"].GetInt(),
				jsonSrc[i]["Name"].GetString(),
				jsonSrc[i]["Description"].GetString());
			_locations.insert(_locations.end(), tmp);
			CreateLevelObjects(tmp, jsonSrc[i]["Objects"]);
		}

		// Add connections 
		int connectedElementId;
		Location* src = nullptr;
		for (int i = 0; i < jsonSrc.Size(); i++)
		{
			if (jsonSrc[i]["ConnectedLocations"].IsArray())
			{
				src = FindLocationById(jsonSrc[i]["Id"].GetInt());
				const Value& connections = jsonSrc[i]["ConnectedLocations"];
				for (int n = 0; n < connections.Size(); n++)
				{
					connectedElementId = connections[n].GetInt();
					if (connectedElementId > -1)
					{
						Location* connectedElement = FindLocationById(connectedElementId);

						if (src != nullptr &&
							connectedElement != nullptr)
						{
							if (connectedElement != nullptr)
							{
								src->AddLocation(connectedElement, n);
							}
						}
					}
				}
			}
		}
	}

	return _locations;
}

LevelReader::LevelReader(string path)
{
	_path = path;
}