#pragma once
#pragma once
#include "BaseCommand.h"
#include "Location.h"
#include "Player.h"
#include "HealthComponent.h"
#include "CommandHelper.h"
#include "MessagingSystem.h"
#include "AttackMessage.h"

class AttackCommand :
	public BaseCommand
{
public:

	void execute(Location* currentLocation = nullptr, string cmdText = "")
	{
		string objName = CommandHelper::GetGameObjectNameFromCommand(cmdText, "attack");
		GameObject* gObj = currentLocation->GetGameObject(objName);

		if (gObj != nullptr)
		{
			MessagingSystem::GetInstance()->SendMessage("player", 
														MessageTypes::Attack,
														new AttackMessage(gObj, currentLocation));
		}
	}
};