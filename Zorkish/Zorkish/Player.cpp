#include "Player.h"

Player::Player(Location *startLocation)
{
	_currentLocation = startLocation;
}

Location* Player::GetCurrentPosition()
{
	return _currentLocation;
}

void Player::MovePlayer(Direction direction)
{
	if (_currentLocation != nullptr)
	{
		Location* targetLocation = _currentLocation->GetLocation(direction);

		if (targetLocation != nullptr)
		{
			_currentLocation = targetLocation;
		}

		cout << "Current location: " << _currentLocation->GetName() << endl;
	}
}

void Player::HandleMessage(enum MessageTypes type, MessageBase* message)
{
	if (type == MessageTypes::Attack)
	{
		GameObject* gameObj = ((AttackMessage*)message)->GetGameObject();
		HealthComponent* health = ((HealthComponent*)gameObj->GetComponent(HealthComponent::type));

		if (health != nullptr)
		{
			health->ChangeHealth(-50);
			health->PrintHealth();

			if (health->GetHealth() <= 0)
			{
				cout << "You killed " << gameObj->GetName() << endl;
				GetCurrentPosition()->RemoveGameObject(gameObj->GetName());
				delete gameObj;
			}
		}
		else
		{
			cout << "You can not attack " << gameObj->GetName() << endl;
		}
	}
}