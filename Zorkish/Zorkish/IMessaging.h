#pragma once
#include "MessageTypes.h"
#include "MessageBase.h"

class Messaging {
public: 
	virtual void HandleMessage(enum MessageTypes type, MessageBase* message) {};
};