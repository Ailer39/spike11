#include "MessagingSystem.h"


static MessagingSystem* instance = nullptr;

MessagingSystem* MessagingSystem::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new MessagingSystem();
	}

	return instance;
}

void MessagingSystem::RegisterObject(string name, Messaging* obj)
{
	if (_registeredObjects.find(name) == _registeredObjects.end())
	{
		_registeredObjects.insert(pair<string, Messaging*>(name, obj));
	}
}

void MessagingSystem::SendMessage(string target,enum MessageTypes type, MessageBase* message)
{
	if (_registeredObjects.find(target) != _registeredObjects.end())
	{
		Messaging*  obj = _registeredObjects.at(target);
		obj->HandleMessage(type, message);
	}
}