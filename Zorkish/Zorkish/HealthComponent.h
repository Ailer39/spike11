#pragma once
#include "ComponentBase.h"
#include <iostream>

using namespace std;

class HealthComponent :
	public ComponentBase
{
private:
	int _health;
public:
	static const string type;
	HealthComponent(int startHealth);
	int GetHealth();
	void ChangeHealth(int amount);
	void PrintHealth();
};