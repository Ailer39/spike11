#pragma once

#include <string>
#include "Location.h"

using namespace std;

class BaseCommand
{
public:
	virtual void execute(Location* currentLocation = nullptr, string cmdText = "") {};
};
