#include "SelectAdventure.h"

static SelectAdventure* instance;

SelectAdventure* SelectAdventure::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new SelectAdventure();
	}

	return instance;
}

void SelectAdventure::Show(StateManager* context)
{
	cout << "Zorkish::Select Adventure" << endl << endl;
	cout << "Choose your adventure:" << endl << endl;
	cout << "(1) Level 1" << endl;
	LevelReader* reader = new LevelReader("..\\..\\Levels.txt");
	vector<Location*> level1 = reader->GetLevelGraph();
	Location* currentLocation = level1[0];
	cout << "Press 0 to go to the main menu" << endl;

	unsigned int input;
	cin >> input;

	if (input == 0)
	{
		context->ChangeState(0);
	}
	else if(level1.size() >= input)
	{
		context->ChangeToGameState(level1[0]);
	}
}