#pragma once

#include <string>
#include <list>
#include <iostream>
#include "GameObject.h"

using namespace std;

class Inventory
{
public:
	Inventory();
	~Inventory();
	void Print();
	void Add(GameObject* item);
	GameObject* UseItem(string itemName);
	void RemoveItem(string itemName);
};