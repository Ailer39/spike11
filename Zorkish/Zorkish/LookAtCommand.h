#pragma once
#include "BaseCommand.h"
#include "Location.h"
#include "Player.h"
#include "CommandHelper.h"

class LookAtCommand :
	public BaseCommand
{
public:

	void execute(Location* currentLocation = nullptr, string cmdText = "")
	{
		string objName = CommandHelper::GetGameObjectNameFromCommand(cmdText, "at");
		GameObject* gObj = currentLocation->GetGameObject(objName);

		if (gObj != nullptr)
		{
			gObj->LookAt();
		}
	}
};